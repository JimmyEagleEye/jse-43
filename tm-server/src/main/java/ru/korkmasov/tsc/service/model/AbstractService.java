package ru.korkmasov.tsc.service.model;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.service.IConnectionService;
import ru.korkmasov.tsc.model.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
