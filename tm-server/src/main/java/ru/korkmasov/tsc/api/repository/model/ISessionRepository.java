package ru.korkmasov.tsc.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.IRepository;
import ru.korkmasov.tsc.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    List<Session> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    void update(final Session session);

}
