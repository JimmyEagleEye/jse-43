package ru.korkmasov.tsc.api.repository;

import ru.korkmasov.tsc.model.AbstractEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IRepository<E extends AbstractEntity> {

    void add(@Nullable E entity);

    @Nullable
    E findById(@NotNull String id);

    void addAll(final Collection<E> collection);

    void clear();

    void remove(@Nullable E entity);

    @Nullable
    E removeById(@NotNull String id);

    int size();

    @NotNull
    List<E> findAll();

}
