package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.endpoint.Task;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import static ru.korkmasov.tsc.util.TerminalUtil.incorrectValue;

public final class TaskByNameUpdateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by name";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(serviceLocator.getSession(), id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task taskUpdated = serviceLocator.getTaskEndpoint().updateTaskById(serviceLocator.getSession(), id, name, description);
        if (taskUpdated == null) incorrectValue();
    }

}
