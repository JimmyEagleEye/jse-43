package ru.korkmasov.tsc.command.user;

import ru.korkmasov.tsc.enumerated.Role;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.util.TerminalUtil;

import static ru.korkmasov.tsc.util.TerminalUtil.nextLine;

public final class UserByLoginUnlockCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        //serviceLocator.getAdminEndpoint().unlockByLogin(getSession(), login);
    }

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
