package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.IPropertyService;
import ru.korkmasov.tsc.api.service.dto.*;

public interface ServiceLocator {

    @NotNull
    ITaskDtoService getTaskDtoService();

    @NotNull
    IProjectDtoService getProjectDtoService();

    @NotNull
    IProjectTaskDtoService getProjectTaskDtoService();

    @NotNull
    IUserDtoService getUserDtoService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionDtoService getSessionDtoService();

    @NotNull
    IDataService getDataService();

    @NotNull
    IConnectionService getConnectionService();
}
