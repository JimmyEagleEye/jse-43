package ru.korkmasov.tsc;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.bootstrap.Bootstrap;
import ru.korkmasov.tsc.dto.UserDto;

public class AbstractTest {

    @NotNull
    protected static final Bootstrap BOOTSTRAP = new Bootstrap();
    @NotNull
    protected static final String TEST_USER_EMAIL = "test@user.email";
    @NotNull
    protected static final String TEST_USER_NAME = "test_user";
    @NotNull
    protected static final String TEST_USER_PASSWORD = "test_user_pass";
    @NotNull
    protected static UserDto TEST_USER;
    @NotNull
    protected static String TEST_USER_ID;

}
