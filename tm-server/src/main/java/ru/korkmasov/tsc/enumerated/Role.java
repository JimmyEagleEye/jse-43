package ru.korkmasov.tsc.enumerated;

import ru.korkmasov.tsc.exception.entity.RoleNotFoundException;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import static ru.korkmasov.tsc.util.ValidationUtil.checkRole;

@Getter
public enum Role {
    USER("UserDTO"),
    ADMIN("Admin");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public static @NotNull Role getRole(String s) {
        s = s.toUpperCase();
        if (!checkRole(s)) throw new RoleNotFoundException();
        return valueOf(s);
    }

}