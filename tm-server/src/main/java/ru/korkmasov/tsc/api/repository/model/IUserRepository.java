package ru.korkmasov.tsc.api.repository.model;

import ru.korkmasov.tsc.api.IRepository;
import ru.korkmasov.tsc.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(final String login);

    User findByEmail(final String email);

    void removeUserByLogin(final String login);

    void update(final User user);

}
